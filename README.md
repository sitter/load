<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>
-->

https://haraldsitter.eu/posts/hugging-websites/

# Server Load Testing

- provision_servers.rb spins up servers and then opens IRB - the servers live as long as the IRB is open
- docker-compose.yaml is a docker-compose file and sets up a 'master' with Selenium Grid and Locust using docker images
- locustfile.py is a Locust test file. It needs to be on all machines that run locust
- test.py is a Selenium test file. It only needs to be on the master and gets distributed automatically to all workers

# Setup

- Put a Digital Ocean token in .token
- Copy config.example.json to config.json
- Edit the config as needed
-- master: the hostname under which the servers can contact the master (e.g. if you host the master locally you need to port forward it to the outside)
- `docker-compose up`
- http://localhost:4444/ui/ shows the selenium grid
- http://localhost:8089/ shows the locust ui

