# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

import os
import json
from locust import HttpUser, between, task

class WebsiteRunner(HttpUser):
    wait_time = between(4, 8)

    config = None
    with open(f'{os.path.dirname(os.path.realpath(__file__))}/config.json', 'r') as data:
        config = json.load(data)

    uri = config['uri']

    def on_start(self):
        self.client.verify = False

    @task
    def root(self):
        self.client.get(f'{self.uri}/')

    @task
    def login(self):
        self.client.post(f'{self.uri}/system/auth/login/', json={
            'username': self.config['username'], 'password': self.config['password']
        })
        self.client.post(f'{self.uri}/system/auth/who-am-i/')
        self.client.post(f'{self.uri}/system/auth/secure/logout/')
