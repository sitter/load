#!/usr/bin/env python3

# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

import unittest
import time
import json
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC

class LoadTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # options = webdriver.FirefoxOptions()
        # options.add_argument("--headless")
        self.driver = webdriver.Remote(
            command_executor='http://localhost:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.FIREFOX)
        self.driver.set_page_load_timeout(16)
        self.driver.set_script_timeout(32)

        with open('config.json', 'r') as data:
            self.config = json.load(data)
        self.uri = self.config['uri']

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_run(self):
        self.driver.get(f'{self.uri}')
        username_field = WebDriverWait(self.driver, 4).until(
            EC.presence_of_element_located((By.ID, 'mat-input-0'))
        )
        username_field.click()
        username_field.send_keys(self.config['username'])
        password_field = self.driver.find_element(By.ID, 'mat-input-1')
        password_field.click()
        password_field.send_keys(self.config['password'])
        self.driver.find_element(By.CLASS_NAME, 'login-button').click()

        # Make sure we are logged in now.
        self.driver.get(f'{self.uri}/2/autopilot')
        WebDriverWait(self.driver, self.driver.timeouts.page_load).until(
            EC.presence_of_element_located((By.TAG_NAME, 'os-autopilot'))
        )

        for i in range(8):
            self.driver.refresh()
            time.sleep(1)

        for i in range(8):
            self.driver.get(f'{self.uri}/2/autopilot')
            WebDriverWait(self.driver, self.driver.timeouts.page_load).until(
               EC.presence_of_element_located((By.TAG_NAME, 'os-autopilot'))
            )
            time.sleep(1)
            self.driver.get(f'{self.uri}/2/agenda')
            WebDriverWait(self.driver, self.driver.timeouts.page_load).until(
               EC.presence_of_element_located((By.TAG_NAME, 'os-agenda-main'))
            )
            time.sleep(1)
            self.driver.get(f'{self.uri}/2/motions')
            WebDriverWait(self.driver, self.driver.timeouts.page_load).until(
               EC.presence_of_element_located((By.TAG_NAME, 'os-motion-list'))
            )
            time.sleep(1)
            self.driver.get(f'{self.uri}/2/assignments')
            WebDriverWait(self.driver, self.driver.timeouts.page_load).until(
               EC.presence_of_element_located((By.TAG_NAME, 'os-assignment-list'))
            )
            time.sleep(1)
            self.driver.get(f'{self.uri}/2/participants')
            WebDriverWait(self.driver, self.driver.timeouts.page_load).until(
               EC.presence_of_element_located((By.TAG_NAME, 'os-participant-main'))
            )
            time.sleep(1)
            self.driver.get(f'{self.uri}/2/mediafiles')
            WebDriverWait(self.driver, self.driver.timeouts.page_load).until(
               EC.presence_of_element_located((By.TAG_NAME, 'os-mediafile-main'))
            )
            time.sleep(1)
            self.driver.get(f'{self.uri}/2/projectors')
            WebDriverWait(self.driver, self.driver.timeouts.page_load).until(
               EC.presence_of_element_located((By.TAG_NAME, 'os-projector-main'))
            )
            time.sleep(4)

if __name__ == '__main__':
    unittest.main()
