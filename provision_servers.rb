#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

require 'droplet_kit'
require 'json'
require 'tty/logger'
require 'tty/command'

LOGGER = TTY::Logger.new

LOCUST_DATA = File.read("#{__dir__}/locustfile.py")
CONFIG_DATA = File.read("#{__dir__}/config.json")
CONFIG = JSON.parse(CONFIG_DATA)

@droplets = []

at_exit do
  binding.irb
  @droplets.each do |droplet|
    @client&.droplets&.delete(id: droplet.id)
  end
end

@client = DropletKit::Client.new(access_token: File.read("#{__dir__}/.token").strip)

user_data = <<~ABC
  #!/bin/bash
  set -ex

  apt-get update
  apt-get -y install docker.io python3-pip
  docker pull selenium/node-firefox
  docker run -d -e SE_EVENT_BUS_HOST=#{CONFIG.fetch('master')} -e SE_EVENT_BUS_PUBLISH_PORT=4442 -e SE_EVENT_BUS_SUBSCRIBE_PORT=4443 -e SE_NODE_HOST=$(ip route get 8.8.8.8 | sed -n '/src/{s/.*src *\\([^ ]*\\).*/\\1/p;q}') -p 5555:5555 selenium/node-firefox

  git clone https://invent.kde.org/sitter/load.git /var/lib/worker
  cat << EOF > /var/lib/worker/config.json
    #{CONFIG_DATA}
  EOF
  # Indentation matters here; it's python!
  cat << EOF > /var/lib/worker/locustfile.py
  #{LOCUST_DATA}
  EOF
  cat << EOF > /etc/systemd/system/worker.service
    # SPDX-License-Identifier: AGPL-3.0-or-later
    # SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

    [Unit]
    Description=Locust Worker

    [Service]
    DynamicUser=yes
    WorkingDirectory=%S/worker
    Environment=HOME=%S/worker
    ExecStartPre=pip install locust
    ExecStart=%S/worker/.local/bin/locust --worker --master-host=#{CONFIG.fetch('master')}
    StateDirectory=worker
  EOF
  systemctl daemon-reload
  systemctl start worker.service
ABC

@client.droplets.all.each do |droplet|
  @client.droplets.delete(id: droplet.id) if droplet.name.start_with?('load')
end

@droplets = []
15.times.collect do |i|
  # NOTE that this may raise an exception. Since we want to clean up all already created instance we'll need to append
  # them at time of creation rather than collect()ing them as that would not collect the temporaries before a possible
  # exception.
  @droplets << @client.droplets.create(
    DropletKit::Droplet.new(
      name: "load#{i}",
      region: 'fra1',
      image: 'ubuntu-22-04-x64',
      size: 'c-16',
      ssh_keys: @client.ssh_keys.all.collect(&:fingerprint),
      private_networking: true,
      ipv6: true,
      user_data: user_data
    ))
end

@droplets.each do |droplet|
  16.times do
    droplet = @client.droplets.find(id: droplet.id)
    raise 'not created yet' unless droplet.public_ip

    TTY::Command.new.run("ssh -o StrictHostKeyChecking=no -o ConnectTimeout=2 root@#{droplet.public_ip} cloud-init status --wait --long")
    LOGGER.success('up', droplet: droplet.name, ip: droplet.public_ip, id: droplet.id)
    break
  rescue => e
    p e
    LOGGER.info('not up yet', droplet: droplet.name, ip: droplet.public_ip, id: droplet.id)
    sleep(32)
  end
end
